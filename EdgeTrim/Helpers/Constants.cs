﻿using System.Reflection;

namespace EdgeTrim.Helpers
{
    class Constants
    {
        public static string APP_VERSION = Assembly.GetExecutingAssembly().GetName().Version.ToString();
        public static string APP_NAME = "Рассечь кромку.";
        public static string MAIN_WINDOW_TITLE = APP_NAME + "     v." + APP_VERSION;

        public static string SETTINGS_FILE_NAME = @"options.xml";

        public static double TRIM_RADIUS = 0.003;
        public static int COMMAND_CONVERT_ENTITIES = 57;


    }
}
