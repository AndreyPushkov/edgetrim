﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdgeTrim.Helpers
{
    public enum LineWidth
    {
        [Description("По умолчанию")]
        DEFAULT = -1,
        [Description("Тонкая")]
        THIN = 0,
        [Description("Нормальная")]
        NORMAL = 1,
        [Description("Утолщенная")]
        THICK = 2,
        [Description("Утолщенная (2)")]
        THICK2 = 3,
        [Description("Утолщенная (3)")]
        THICK3 = 4,
        [Description("Утолщенная (4)")]
        THICK4 = 5,
        [Description("Утолщенная (5)")]
        THICK5 = 6,
        [Description("Утолщенная (6)")]
        THICK6 = 7
    }

    public enum LineStyle
    {
        [Description("По умолчанию")]
        DEFAULT = 7,
        [Description("Сплошная")]
        SOLID = 0,
        [Description("Невидимая")]
        HIDDEN = 1,
        [Description("Осевая")]
        CENTER = 4,
    }

}
/*
swLineCENTER	4
swLineCHAIN	3
swLineCHAINTHICK	6 = Thin/Thick Chain
swLineCONTINUOUS	0 = Solid
swLineDEFAULT	7
swLineHIDDEN	1 = Dashed
swLinePHANTOM	2
swLineSTITCH	5
*/
