﻿using System;

namespace EdgeTrim.Models
{
    [Serializable]
    public class SettingsModel
    {
        public double TrimGap { get; set; } = 6;
        public int LineWidth { get; set; } = 2;
        public int LineStyle { get; set; } = 0;
    }
}
