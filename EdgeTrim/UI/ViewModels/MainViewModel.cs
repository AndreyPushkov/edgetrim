﻿using BatchSaveSettings.Services;
using EdgeTrim.Helpers;
using EdgeTrim.Services;
using EdgeTrim.UI.Helpers;
using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using System;
using System.Windows.Input;
using static EdgeTrim.Helpers.Constants;

namespace EdgeTrim.UI.ViewModels
{
    public class MainViewModel : BaseViewModel, IDisposable
    {
        public delegate void MainWindowEvent();
        public event MainWindowEvent MainWindowClose_Notify;

        public string MainTitle => MAIN_WINDOW_TITLE;
        TrimService trimService;
        LineWidth lineWidth = LineWidth.NORMAL;
        LineStyle lineStyle = LineStyle.HIDDEN;

        string info = "Выберите кромку или линию эскиза.";

        public MainViewModel(SldWorks app)
        {
            trimService = new TrimService(app);
            CloseCommand = new DelegateCommand((obj) => Close());

            //Width = LineWidth.NORMAL;
            //Style = LineStyle.HIDDEN;
        }

        public string Info { get => info; set => SetProperty(ref info, value); }
        public double Gap {
            get
            {
                return App.Settings.TrimGap;
            }
            set
            {
                App.Settings.TrimGap = value;
                OnPropertyChanged(nameof(Gap));
                SaveSettiings();
            }
        }
        public LineWidth Width
        {
            get => (LineWidth)App.Settings.LineWidth;
            set
            {
                App.Settings.LineWidth = (int)value;
                OnPropertyChanged(nameof(Width));
                SaveSettiings();
            }
        }
        public LineStyle Style
        {
            get => (LineStyle)App.Settings.LineStyle;
            set
            {
                App.Settings.LineStyle = (int)value;
                OnPropertyChanged(nameof(Style));
                SaveSettiings();
            }
        }


        #region METHODS

        void SaveSettiings()
        {
            SettingsService.SaveSettings(App.Settings);
        }

        void Close()
        {
            MainWindowClose_Notify?.Invoke();
        }


        public void Trim(object edge, swSelectType_e type)
        {
            trimService?.Trim(edge, type);
        }

        public void Trim(SketchSegment segment)
        {
            trimService?.Trim(segment);
        }

        public void Dispose()
        {
            trimService?.Dispose();
            trimService = null;
        }

        #endregion

        #region Commands
        public ICommand CloseCommand { get; private set; }
        #endregion


    }
}
