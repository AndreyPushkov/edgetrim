﻿using EdgeTrim.Helpers;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Data;

namespace EdgeTrim.UI.Converters
{
    internal class EnumStyleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Enum myEnum = (Enum)value;
            string description = GetDescription(myEnum);
            return description;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return GetValueByDescription(value as string);
        }

        public static string GetDescription(Enum enumValue)
        {
            if (!(enumValue is Enum))
                return null;

            var description = enumValue.ToString();
            var fieldInfo = enumValue.GetType().GetField(enumValue.ToString());

            if (fieldInfo != null)
            {
                var attrs = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                {
                    description = ((DescriptionAttribute)attrs[0]).Description;
                }
            }

            return description;
        }

        public static LineStyle GetValueByDescription(string description)
        {
            foreach (var field in typeof(LineStyle).GetFields())
            {
                if (Attribute.GetCustomAttribute(field,
                typeof(DescriptionAttribute)) is DescriptionAttribute attribute)
                {
                    if (attribute.Description == description)
                        return (LineStyle)field.GetValue(null);
                }
                else
                {
                    if (field.Name == description)
                        return (LineStyle)field.GetValue(null);
                }
            }

            //throw new ArgumentException("Not found.", nameof(description));
            // Or
            return default(LineStyle);
        }
    }
}
