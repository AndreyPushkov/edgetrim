﻿using BatchSaveSettings.Services;
using EdgeTrim.Exceptions;
using EdgeTrim.Helpers;
using EdgeTrim.Models;
using EdgeTrim.Sw;
using EdgeTrim.UI.ViewModels;
using EdgeTrim.UI.Views;
using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using System;
using System.Windows;

namespace EdgeTrim
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        MainWindow window;
        public static MainViewModel viewModel;

        public static SwApp swApp;

        public static SettingsModel Settings;

        protected override void OnStartup(StartupEventArgs e)
        {
            AppUtils.CheckApplicationSingleInstance();
            try
            {
                swApp = AppUtils.GetSwInstance();

                Settings = SettingsService.ReadSettings();

                window = new MainWindow();
                viewModel = new MainViewModel(swApp.SwApplication);
                window.DataContext = viewModel;
                
                window.Closed += Window_Closed;
                viewModel.MainWindowClose_Notify += CloseMain;

                bool attachResult = swApp.AttachEventHandlers();
                if (!attachResult)
                    throw new CustomException("Ошибка инициации слушателей SolisWorks....");
                window.Show();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            CloseMain();
        }

        public void CloseMain()
        {
            if (window != null)
            {
                swApp.DetachEventHandlers();
                swApp.Dispose();
                viewModel.Dispose();
                window.Close();
            }
            viewModel = null;
            window = null;
            swApp = null;
        }

        public void Trim(object edge, swSelectType_e type)
        {
            viewModel?.Trim(edge, type);
        }

        public void Trim(SketchSegment segment)
        {
            viewModel?.Trim(segment);
        }
    }
}
