﻿using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using System;
using System.Linq;

using static EdgeTrim.Helpers.Constants;

using static EdgeTrim.Helpers.Constants;

namespace EdgeTrim.Services
{
    internal class TrimService : IDisposable
    {
        SldWorks swApp;
        ModelDoc2 modelDoc;
        ModelDocExtension modelDocExtension;
        DrawingDoc drawDoc;
        SelectionMgr selectionMgr;
        View selectedView;
        SelectData selectData;
        MathUtility mathUtils;
        MathTransform modelTransform;
        SketchManager sketchManager;


        double[] selectionPoint;
        double[] viewPosition;


        public bool InWorks { get; set; } = false;

        public TrimService(SldWorks swApp)
        {
            this.swApp = swApp;
        }



        public void Trim(object edge, swSelectType_e type)
        {
            InWorks = true;
            swApp.CommandInProgress = true;

            mathUtils = swApp.GetMathUtility();


            if (type == swSelectType_e.swSelSILHOUETTES)
            {
                edge = (SilhouetteEdge)edge;
            }
            else
            {
                edge = (Edge)edge;
            }

            Init();
            double[] segmentsCoords = GetEdgePointsCoordinates(edge, type);

            modelDocExtension.RunCommand(57, "");
            modelDoc.ClearSelection2(true);

            selectionMgr.AddSelectionListObject(edge, selectData);
            drawDoc.HideEdge();
            modelDoc.ClearSelection2(true);

            SketchManager sketchManager = modelDoc.SketchManager;

            SketchSegment tmpSegment = sketchManager.CreateCircleByRadius(
               (selectionPoint[0] - viewPosition[0]) / modelTransform.ArrayData[12],
               (selectionPoint[1] - viewPosition[1]) / modelTransform.ArrayData[12],
               selectionPoint[2] / modelTransform.ArrayData[12],
               App.Settings.TrimGap / (modelTransform.ArrayData[12] * 2000));
            modelDoc.ClearSelection2(true);

            bool selectionStatus1 = modelDocExtension.SelectByID2("", "SKETCHSEGMENT",
                segmentsCoords[0],
                segmentsCoords[1],
                segmentsCoords[2],
                false, -1, null, 0);


            bool status = sketchManager.SketchTrim(
                (int)swSketchTrimChoice_e.swSketchTrimClosest,
                (selectionPoint[0] - viewPosition[0]) / modelTransform.ArrayData[12],
                (selectionPoint[1] - viewPosition[1]) / modelTransform.ArrayData[12],
                selectionPoint[2] / modelTransform.ArrayData[12]);

            modelDoc.ClearSelection2(true);

            bool selectionStatus = modelDocExtension.SelectByID2("", "SKETCHSEGMENT",
                segmentsCoords[0],
                segmentsCoords[1],
                segmentsCoords[2],
                false, -1, null, 0);

            if (selectionStatus)
            {
                if (selectionMgr.GetSelectedObjectType3(1, -1) == (int)swSelectType_e.swSelSKETCHSEGS)
                {
                    SketchSegment selectedSegment = (SketchSegment)selectionMgr.GetSelectedObject6(1, -1);

                    selectedSegment.Width = App.Settings.LineWidth;
                    selectedSegment.Style = App.Settings.LineStyle;
                }
            }
            modelDoc.ClearSelection2(true);

            selectionStatus = modelDocExtension.SelectByID2("", "SKETCHSEGMENT",
                segmentsCoords[3],
                segmentsCoords[4],
                segmentsCoords[5],
                false, -1, null, 0);

            if (selectionStatus)
            {
                if (selectionMgr.GetSelectedObjectType3(1, -1) == (int)swSelectType_e.swSelSKETCHSEGS)
                {
                    SketchSegment selectedSegment = (SketchSegment)selectionMgr.GetSelectedObject6(1, -1);

                    selectedSegment.Width = App.Settings.LineWidth;
                    selectedSegment.Style = App.Settings.LineStyle;
                }
            }
            modelDoc.ClearSelection2(true);

            selectionMgr.AddSelectionListObject(tmpSegment, selectData);
            modelDocExtension.DeleteSelection2(4);

            drawDoc.ActivateSheet((drawDoc.GetCurrentSheet() as Sheet).GetName());

            swApp.CommandInProgress = false;

            InWorks = false;
        }

        public void Trim(SketchSegment segment)
        {
            if (!InWorks)
            {
                swApp.CommandInProgress = true;

                InWorks = true;
                swApp.CommandInProgress = true;

                Init();

                segment.Width = App.Settings.LineWidth;
                segment.Style = App.Settings.LineStyle;

                SketchManager sketchManager = modelDoc.SketchManager;

                SketchSegment tmpSegment = sketchManager.CreateCircleByRadius(
                    selectionPoint[0],
                    selectionPoint[1],
                    selectionPoint[2],
                    App.Settings.TrimGap / (modelTransform.ArrayData[12] * 2000));

                selectionMgr.AddSelectionListObject(segment, selectData);

                bool status = sketchManager.SketchTrim(
                    (int)swSketchTrimChoice_e.swSketchTrimClosest,
                    selectionPoint[0],
                    selectionPoint[1],
                    selectionPoint[2]);

                modelDoc.ClearSelection2(true);
                selectionMgr.AddSelectionListObject(tmpSegment, selectData);
                modelDocExtension.DeleteSelection2(4);
                modelDoc.ClearSelection2(true);
                drawDoc.ActivateSheet((drawDoc.GetCurrentSheet() as Sheet).GetName());
                swApp.CommandInProgress = false;
                InWorks = false;
            }
        }

        void Init()
        {
            modelDoc = swApp.ActiveDoc;
            modelDocExtension = modelDoc.Extension;
            drawDoc = modelDoc as DrawingDoc;


            selectionMgr = modelDoc.SelectionManager;
            selectData = selectionMgr.CreateSelectData();

            selectionPoint = selectionMgr.GetSelectionPoint2(1, -1);

            selectedView = selectionMgr.GetSelectedObjectsDrawingView2(1, -1);
            modelTransform = selectedView.ModelToViewTransform;
            viewPosition = selectedView.Position;

            drawDoc.ActivateView(selectedView.Name);

        }

        double[] GetEdgePointsCoordinates(object edge, swSelectType_e type)
        {
            MathPoint modelStartPoint = null;
            MathPoint modelEndPoint = null;
            Entity entity = null;
            Component2 selComponent = null;
            if (type == swSelectType_e.swSelSILHOUETTES)
            {
                modelStartPoint = (edge as SilhouetteEdge).GetStartPoint();
                modelEndPoint = (edge as SilhouetteEdge).GetEndPoint();
                Face2 face = (edge as SilhouetteEdge).GetFace();
                entity = (Entity)face;
            }
            else
            {
                double[] startPoint = (edge as Edge).GetCurveParams3().StartPoint;
                double[] endPoint = (edge as Edge).GetCurveParams3().EndPoint;
                modelStartPoint = mathUtils.CreatePoint(startPoint);
                modelEndPoint = mathUtils.CreatePoint(endPoint);
                entity = (Entity)edge;
            }

            selComponent = entity.GetComponent();
            MathTransform compTransform;
            if (selComponent != null)
            {
                compTransform = selComponent.Transform2;
                modelStartPoint = modelStartPoint.MultiplyTransform(compTransform);
                modelEndPoint = modelEndPoint.MultiplyTransform(compTransform);
            }

            MathPoint drawingStartPoint = modelStartPoint.MultiplyTransform(modelTransform);
            MathPoint drawingEndPoint = modelEndPoint.MultiplyTransform(modelTransform);

            double[] drawingStartPointArray = drawingStartPoint.ArrayData;
            double[] drawingEndPointArray = drawingEndPoint.ArrayData;

            return drawingStartPointArray.Concat(drawingEndPointArray).ToArray();
        }


        public void Dispose()
        {
            //bool st = modelDocExtension.EditRebuildAll();

            viewPosition = null;
            selectionPoint = null;
            modelDoc = null;
            modelDocExtension = null;
            drawDoc = null;
            selectionMgr = null;
            selectedView = null;
            selectData = null;
            modelTransform = null;
            sketchManager = null;
            swApp = null;
        }
    }
}
