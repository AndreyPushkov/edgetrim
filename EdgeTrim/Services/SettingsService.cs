﻿using EdgeTrim.Models;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Xml.Serialization;

using static EdgeTrim.Helpers.Constants;

namespace BatchSaveSettings.Services
{
    internal class SettingsService
    {
        private static string GetPdfSettingsPath()
        {
            string path = Assembly.GetExecutingAssembly().Location;
            FileInfo fi = new FileInfo(path);
            string dirPath = fi.DirectoryName;
            return dirPath + @"\" + SETTINGS_FILE_NAME;
        }

        public static SettingsModel ReadSettings()
        {
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(SettingsModel));
                using (FileStream fs = new FileStream(GetPdfSettingsPath(), FileMode.Open))
                {
                    return xmlSerializer.Deserialize(fs) as SettingsModel;
                }
            }
            catch (Exception e)
            {
                Debug.Print(" Settings error: {0}", e.Message);
                MessageBox.Show("Ошибка чтения файла настроек " + GetPdfSettingsPath() + ".\n\rИспользованы настройки по умолчанию.");
                SaveSettings( new SettingsModel());
                return new SettingsModel();
            }
        }

        public static void SaveSettings(SettingsModel settings)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(settings.GetType());
            using (FileStream fs = new FileStream(GetPdfSettingsPath(), FileMode.Create))
            {
                xmlSerializer.Serialize(fs, settings);
            }
        }

    }
}
